package com.coherentlogic.cmr

import com.coherentlogic.cmr.common.QueryBuilderDecorator
import scala.collection.mutable.Seq

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.SparkSession

import scala.collection.mutable.MutableList

import scala.math.BigDecimal

// value foreach is not a member of java.util.List
import scala.collection.JavaConversions._
import scala.compat.java8.FunctionConverters._

import java.util.Collection

import com.jamonapi.Monitor
import com.jamonapi.MonitorFactory

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.apache.spark.sql.Encoders
import org.apache.spark.sql.Encoder

import com.coherentlogic.quandl.client.core.domain.timeseries.data.Row

import com.coherentlogic.cmr._

/**
 * 
 */
package object quandl {

  /**
import scala.collection.mutable.MutableList
import scala.compat.java8.FunctionConverters._
import com.coherentlogic.cmr.api.builders.CMR
import com.coherentlogic.cmr.quandl._
import com.coherentlogic.quandl.client.core.domain.timeseries.ColumnNames

val QUANDL_API_KEY=System.getenv ("QUANDL_API_KEY")

val cmr = new CMR ()

case class EODHD (columnName : String) {}
def mapper (quandlResponse : com.coherentlogic.quandl.client.core.domain.timeseries.metadata.QuandlResponse) : scala.collection.mutable.Seq[EODHD] = {
  val result : MutableList[EODHD] = MutableList()

  val dataset = quandlResponse.getDataset

  val columnNames = dataset.getColumnNames();


  columnNames.forEachColumnName (
    asJavaConsumer[String] (
      columnName => {
        result += new EODHD (columnName)
      }
    )
  )

  result
}

import spark.implicits._

val eodHDSeq : Seq[EODHD] = cmr.quandl.metadata.datasets("EOD", "HD").metadata().withApiKey(QUANDL_API_KEY).doGetAsSeq (mapper)

val eodHDDS = eodHDSeq.toDS

eodHDDS.show(5)
eodHDDS.count

   */
  implicit class QuandlTimeSeriesMetadataQueryBuilderDecorator (
    queryBuilder : com.coherentlogic.quandl.client.core.builders.timeseries.metadata.QueryBuilder
  ) extends QueryBuilderDecorator[com.coherentlogic.quandl.client.core.domain.timeseries.metadata.QuandlResponse] {

    // TODO: Refactor this as this method should be unnecessary.
/*    def doGetAsSeq[T] (
      quandlResponse : com.coherentlogic.quandl.client.core.domain.timeseries.metadata.QuandlResponse,
      mapper : (com.coherentlogic.quandl.client.core.domain.timeseries.metadata.QuandlResponse) => Seq[T]
    ) : Seq[T] = {

      val result = mapper (quandlResponse)

      result
    }*/

    def doGetAsSeq[T] (
      mapper : (com.coherentlogic.quandl.client.core.domain.timeseries.metadata.QuandlResponse) => Seq[T]
    ) : Seq[T] = {

      val monitor = MonitorFactory.start(
        "com.coherentlogic.cmr.quandl.QuandlTimeSeriesMetadataQueryBuilderDecorator.doGetAsSeq")

      val quandlResponse = queryBuilder.doGetAsQuandlResponse ()

      val mutableResult : Seq[T] = doGetAsSeq (quandlResponse, mapper)

      monitor.stop()

      val message = s"Method performance: $monitor"

      printAndLog (message)

      //val immutableResult = scala.collection.immutable.Seq(mutableResult : _*)

      mutableResult
    }
  }

  /**
import scala.collection.mutable.MutableList
import scala.compat.java8.FunctionConverters._
import com.coherentlogic.cmr.api.builders.CMR
import com.coherentlogic.cmr.quandl._
import com.coherentlogic.quandl.client.core.domain.tables.Column
import com.coherentlogic.quandl.client.core.domain.tables.Columns
import com.coherentlogic.quandl.client.core.domain.tables.Datum
import com.coherentlogic.quandl.client.core.domain.tables.DatumArray

val QUANDL_API_KEY=System.getenv ("QUANDL_API_KEY")

val cmr = new CMR ()

case class MerF1 (reportDate : java.sql.Date, amount : scala.math.BigDecimal) {}
def mapper (quandlResponse : com.coherentlogic.quandl.client.core.domain.tables.QuandlResponse) : scala.collection.mutable.Seq[MerF1] = {

  var result : MutableList[MerF1] = MutableList()

  var dataTable : com.coherentlogic.quandl.client.core.domain.tables.DataTable = quandlResponse.getDataTable

  val columns : Columns = dataTable.getColumns();

  val columnMap = columns.asMap();

  val reportDateColumn : Column = columnMap.get("reportdate");
  val amountColumn : Column = columnMap.get("amount");

  val reportDateIndex = columns.getIndexOf (reportDateColumn)
  val amountIndex = columns.getIndexOf (amountColumn)

  dataTable.getData.forEachDatumArray (
    asJavaConsumer[com.coherentlogic.quandl.client.core.domain.tables.DatumArray] (
      (datumArray : com.coherentlogic.quandl.client.core.domain.tables.DatumArray) => {

        val reportDateDatum : Datum = datumArray.getDatumAt(reportDateIndex);
        val amountDatum : Datum = datumArray.getDatumAt(amountIndex);

        val reportSQLDate = reportDateDatum.getValueAsSQLDate ()
        val amount = new scala.math.BigDecimal(amountDatum.getValueAsBigDecimal ())

        result += new MerF1 (reportSQLDate, amount)
      }
    )
  )
  result
}

import spark.implicits._

val merF1Seq : Seq[MerF1] = cmr.quandl.tables.datatables("MER", "F1").withApiKey(QUANDL_API_KEY).doGetAsSeq (mapper)

merF1Seq.toDS.show(5)
merF1Seq.toDS.count
   */
  implicit class QuandlTablesQueryBuilderDecorator (
    queryBuilder : com.coherentlogic.quandl.client.core.builders.tables.QueryBuilder
  ) extends QueryBuilderDecorator[com.coherentlogic.quandl.client.core.domain.tables.QuandlResponse] {

/*    def doGetAsSeq[T] (
      quandlResponse : com.coherentlogic.quandl.client.core.domain.tables.QuandlResponse,
      mapper : (com.coherentlogic.quandl.client.core.domain.tables.QuandlResponse) => Seq[T]
    ) : Seq[T] = {

      val result = mapper (quandlResponse)

      result
    }*/

    def doGetAsSeq[T] (
      mapper : (com.coherentlogic.quandl.client.core.domain.tables.QuandlResponse) => Seq[T]
    ) : Seq[T] = {

      val monitor = MonitorFactory.start(
        "com.coherentlogic.cmr.quandl.QuandlTablesQueryBuilderDecorator.doGetAsSeq")

      val quandlResponse = queryBuilder.doGetAsQuandlResponse ()

      val mutableResult : Seq[T] = doGetAsSeq (quandlResponse, mapper)

      monitor.stop()

      val message = s"Method performance: $monitor"

      printAndLog (message)

      //val immutableResult = scala.collection.immutable.Seq(mutableResult : _*)

      mutableResult
    }
  }

  implicit class QuandlTimeSeriesDataQueryBuilderDecorator (
    queryBuilder : com.coherentlogic.quandl.client.core.builders.timeseries.data.QueryBuilder
  ) extends QueryBuilderDecorator[com.coherentlogic.quandl.client.core.domain.timeseries.data.Data] {

    /**
     * TODO: We should not need to override this method so refactor this and move the forEachRow into the
     *       mapper or the calling method.
     */
    def doGetAsSeq[T] (
      data : com.coherentlogic.quandl.client.core.domain.timeseries.data.Data,
      mapper : (com.coherentlogic.quandl.client.core.domain.timeseries.data.Row) => T
    ) : MutableList[T] = {

      val result : MutableList[T] = MutableList()

      data.forEachRow(
        asJavaConsumer[com.coherentlogic.quandl.client.core.domain.timeseries.data.Row] (
          (row : com.coherentlogic.quandl.client.core.domain.timeseries.data.Row) => {

            val next = mapper (row)

            if (next != null)
              result += next
          }
        )
      )

      result
    }

    /**
     * This executes the given query and then uses the rowMapper function to convert the result into a list
     * of type T case classes. The result returned from this method can then be converted into a dataset or
     * dataframe.
     * <code>
     * @param rowMapper Function takes an instance of Row and converts it to result of type T and returns the
     *  result. Note that T *must be* a case class.
     *
     * import com.coherentlogic.cmr.quandl._
     * import com.coherentlogic.quandl.client.core.domain.timeseries.data.Row
     * import com.coherentlogic.cmr.api.builders.CMR
     *
     * val cmr = new CMR ()
     *
     * val QUANDL_API_KEY=System.getenv ("QUANDL_API_KEY")
     *
     * case class IceB1 (date : java.sql.Date, open : scala.math.BigDecimal, high : scala.math.BigDecimal,
     *   low : scala.math.BigDecimal) {}
     *
     * def rowMapper (row : Row) : IceB1 = {
     *
     *   var result : IceB1 = null
     *
     *   if (row.hasValuesFor ("Date", "Open")) {
     *
     *     val javaDate = row.getColumnAsDate ("Date")
     *     val javaSQLDate = new java.sql.Date (javaDate.getTime)
     *     val open = row.getColumnAsBigDecimal ("Open")
     *     val scalaBigDecimalOpen = new scala.math.BigDecimal(open)
     *     val high = row.getColumnAsBigDecimal ("High")
     *     val scalaBigDecimalHigh = new scala.math.BigDecimal(high)
     *     val low = row.getColumnAsBigDecimal ("Low")
     *     val scalaBigDecimalLow = new scala.math.BigDecimal(low)
     *
     *     result = new IceB1 (javaSQLDate, scalaBigDecimalOpen, scalaBigDecimalHigh, scalaBigDecimalLow)
     *   }
     * 
     *   result
     * }
     *
     * import spark.implicits._
     *
     * val iceB1List = cmr.quandl.data.datasets("CHRIS", "ICE_B1").data().withApiKey(QUANDL_API_KEY).doGetAsSeq (rowMapper)
     *
     * val result = iceB1List.toDS
     * iceB1List.toDS.show
     *
     * </code>
     */
    def doGetAsSeq[T] (mapper : (Row) => T) : MutableList[T] = {

      val monitor = MonitorFactory.start(
        "com.coherentlogic.cmr.quandl.QuandlTimeSeriesDataQueryBuilderDecorator.doGetAsSeq")

      val quandlResponse = queryBuilder.doGetAsQuandlResponse ()

      def data = quandlResponse.getDatasetData ().getData ()

      val result : MutableList[T] = doGetAsSeq (data, mapper)

      monitor.stop()

      val message = s"Method performance: $monitor"

      printAndLog (message)

      result
    }
  }
}