package com.coherentlogic.cmr.api.builders

import org.springframework.context.ConfigurableApplicationContext
import com.coherentlogic.cmr.api.boot.Main

import java.awt.Desktop;
import java.io.IOException
import java.net.URI
import com.coherentlogic.coherent.data.adapter.core.exceptions.IORuntimeException

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.cmr.api.exceptions.GenericCMRRuntimeException

import java.awt.AWTError

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
class CMR {

  private final val log = LoggerFactory.getLogger(classOf[CMR]);

  final val main : Main = Main.initialize

  def lei () : com.coherentlogic.coherent.data.adapter.lei.client.core.builders.QueryBuilder = {
    return main.getBean (classOf[com.coherentlogic.coherent.data.adapter.lei.client.core.builders.QueryBuilder])
  }

  def fred () : com.coherentlogic.fred.client.core.builders.QueryBuilder = {
    return main.getBean(classOf[com.coherentlogic.fred.client.core.builders.QueryBuilder])
  }

  def openFIGI () : com.coherentlogic.coherent.data.adapter.openfigi.client.core.builders.QueryBuilder = {
    return main.getBean (classOf[com.coherentlogic.coherent.data.adapter.openfigi.client.core.builders.QueryBuilder])
  }

  def treasuryDirect () : com.coherentlogic.treasurydirect.client.core.builders.QueryBuilder = {
    return main.getBean (classOf[com.coherentlogic.treasurydirect.client.core.builders.QueryBuilder])
  }

  def worldBank () : com.coherentlogic.wb.client.core.builders.QueryBuilder = {
    return main.getBean (classOf[com.coherentlogic.wb.client.core.builders.QueryBuilder])
  }

  def quandl () : QuandlBeans = {
    return new QuandlBeans ()
  }

  class QuandlBeans {

    def data () :  com.coherentlogic.quandl.client.core.builders.timeseries.data.QueryBuilder = {
      return main.getBean(classOf[com.coherentlogic.quandl.client.core.builders.timeseries.data.QueryBuilder])
    }

    def metadata () : com.coherentlogic.quandl.client.core.builders.timeseries.metadata.QueryBuilder = {
      return main.getBean(classOf[com.coherentlogic.quandl.client.core.builders.timeseries.metadata.QueryBuilder])
    }

    def tables () : com.coherentlogic.quandl.client.core.builders.tables.QueryBuilder = {
      return main.getBean(classOf[com.coherentlogic.quandl.client.core.builders.tables.QueryBuilder])
    }
  }

  class Man {

    private def openURI (uriText : String) : Unit = {

      val uri = new URI (uriText)

      val exceptionText = s"Unable to open the uri '$uri'; note that this method can only be called when running Spark with a desktop UI. You can copy/paste the URI directly into a browser, however."

      if (Desktop.isDesktopSupported()) {
        try {
          Desktop.getDesktop().browse(uri)
        } catch {
          case awtError : AWTError  => throw new GenericCMRRuntimeException (exceptionText, awtError)
          case   thrown : Throwable => throw new GenericCMRRuntimeException (exceptionText, thrown)
        }
      } else {

        val message = s"This method can only be called when running Spark on a desktop so if you're running this in a terminal window, please navigate to the following URI: $uri"

        log.error (message)

        System.out.println (message)
      }
    }

    def cmr () {
      openURI ("https://coherentlogic.com/wordpress/cmr?coordinates='com.coherentlogic.cmr:api:2.0.3-RELEASE'")
    }

    def java () {
      openURI ("https://docs.oracle.com/javase/")
    }

    def scala () {
      openURI ("https://www.scala-lang.org/api/current/")
    }

    def spark () {
      openURI ("https://spark.apache.org/docs/latest/")
    }
  }

  def man () : Man = {
    return new Man ()
  }
}