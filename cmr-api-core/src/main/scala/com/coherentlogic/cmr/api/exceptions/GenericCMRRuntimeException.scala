package com.coherentlogic.cmr.api.exceptions

import org.springframework.core.NestedRuntimeException

class GenericCMRRuntimeException(msg:String, cause:Throwable) extends NestedRuntimeException (msg, cause) {

//    def this (msg : String) {
//        super(msg);
//    }
//
//    GenericCMRRuntimeException(String msg, Throwable cause) {
//        super(msg, cause);
//    }
}
