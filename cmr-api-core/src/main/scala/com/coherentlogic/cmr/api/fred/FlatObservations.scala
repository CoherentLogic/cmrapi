package com.coherentlogic.cmr.api.fred

import java.util.Date

case class FlatObservations (
    observationsRealtimeStart : Date,
    observationsRealtimeEnd : Date,
    observationStart : Date,
    observationEnd : Date,
    sortOrder : String,
    orderBy : String,
    limit : Long,
    offset : Integer,
    units : String,
    outputType : String,
    fileType : String,
    count : Integer, // ---------------------
    realtimeStart : Date,
    realtimeEnd : Date,
    date: Date,
    value : BigDecimal
) {

}