package com.coherentlogic.cmr.api.fred

import java.util.Date

case class FlatObservation (realtimeStart : Date, realtimeEnd : Date, date: Date, value : BigDecimal) {
  
}