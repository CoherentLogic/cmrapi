package com.coherentlogic.cmr

import com.coherentlogic.cmr.common.QueryBuilderDecorator
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean

import scala.collection.mutable.MutableList

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.SparkSession

import java.sql.Date

import scala.collection.JavaConversions._
import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.mutable.Buffer

import java.text.SimpleDateFormat

import com.coherentlogic.wb.client.core.domain.LendingTypes
import com.coherentlogic.wb.client.core.domain.LendingType
import com.coherentlogic.wb.client.core.domain.PaginationBean
import com.coherentlogic.wb.client.core.domain.IdentityValueBean
import com.coherentlogic.wb.client.core.domain.IndicatorTopic
import com.coherentlogic.wb.client.core.domain.Source

import com.coherentlogic.wb.client.core.builders.QueryBuilder
import com.coherentlogic.wb.client.core.builders.IndicatorsColumnType

/**
 *
 */
package object wb {

  implicit class WorldBankQueryBuilderDecorator  (queryBuilder : QueryBuilder)
    extends QueryBuilderDecorator[SerializableBean] {

    def doGetAsDataPointsDataset (spark : SparkSession) : Dataset[FlatDataPoints] = {

      import spark.implicits._

      var result = MutableList[FlatDataPoints]()

      val dataPoints = queryBuilder.doGetAsDataPoints()

      val dataPointList = dataPoints.getDataPointList ()

      for (dataPoint <- dataPointList) {
        result += new FlatDataPoints (
            dataPoint.getDataPointIndicator().getId,
            dataPoint.getDataPointIndicator().getValue,
            dataPoint.getDataPointCountry().getId,
            dataPoint.getDataPointCountry().getValue,
            dataPoint.getValue,
            dataPoint.getDecimal
        )
      }

      return result.toDS
    }

    private def doGetAsFlatPaginatedIdentityValueBeanDataset (spark : SparkSession, paginationBean : PaginationBean, identityValueBeans : Buffer[IdentityValueBean]) :
      Dataset[FlatPaginatedIdentityValueBean] = {

      import spark.implicits._

      val result = MutableList[FlatPaginatedIdentityValueBean]()

      for (identityValueBean <- identityValueBeans) {
        result += new FlatPaginatedIdentityValueBean (
            paginationBean.getPage,
            paginationBean.getPages,
            paginationBean.getPerPage,
            paginationBean.getTotal,
            identityValueBean.getId,
            identityValueBean.getValue
        )
      }

      return result.toDS
    }

    def doGetAsLendingTypesDataset (spark : SparkSession) : Dataset[FlatPaginatedIdentityValueBean] = {

      val lendingTypes = queryBuilder.doGetAsLendingTypes()

      val lendingTypeList : Buffer[IdentityValueBean] = asScalaBuffer(lendingTypes.getLendingTypeList.asInstanceOf[java.util.List[IdentityValueBean]])

      return doGetAsFlatPaginatedIdentityValueBeanDataset (spark, lendingTypes, lendingTypeList)
    }

    def doGetAsTopicsDataset (spark : SparkSession) : Dataset[FlatPaginatedIdentityValueBean] = {

      val topics = queryBuilder.doGetAsTopics()

      val topicList : Buffer[IdentityValueBean] = asScalaBuffer(topics.getTopicList.asInstanceOf[java.util.List[IdentityValueBean]])

      return doGetAsFlatPaginatedIdentityValueBeanDataset (spark, topics, topicList)
    }

    private def asFlatIndicators (indicatorName : String, indicatorSourceNote : String, indicatorSourceOrganization : String, indicatorId : String, source : Source) : FlatIndicators = {

      val id = source.getId
      val value = source.getValue

      return new FlatIndicators (IndicatorsColumnType.source.toString(), indicatorName, indicatorSourceNote, indicatorSourceOrganization, indicatorId, source.getId, source.getValue, null, null)
    }

    private def asFlatIndicators (indicatorName : String, indicatorSourceNote : String, indicatorSourceOrganization : String, indicatorId : String, indicatorTopic : IndicatorTopic) : FlatIndicators = {

      val id = indicatorTopic.getId
      val value = indicatorTopic.getValue

      return new FlatIndicators(IndicatorsColumnType.indicatorTopic.toString(), indicatorName, indicatorSourceNote, indicatorSourceOrganization, indicatorId, null, null, indicatorTopic.getId, indicatorTopic.getValue)
    }

    def doGetAsIndicatorsDataset (spark : SparkSession) : Dataset[FlatIndicators] = {

      import spark.implicits._

      val indicators = queryBuilder.doGetAsIndicators()

      val results = MutableList[FlatIndicators]()

      val indicatorList = indicators.getIndicatorList

      for (indicator <- indicatorList) {

        val indicatorName = indicator.getName
        val indicatorSourceNote = indicator.getSourceNote
        val indicatorSourceOrganization = indicator.getSourceOrganization
        val indicatorId = indicator.getId

        results += asFlatIndicators (indicatorName, indicatorSourceNote, indicatorSourceOrganization, indicatorId, indicator.getSource)

        val indicatorTopicList = indicator.getIndicatorTopics.getIndicatorTopicList ()

        for (indicatorTopic <- indicatorTopicList)
            results += asFlatIndicators (indicatorName, indicatorSourceNote, indicatorSourceOrganization, indicatorId, indicatorTopic)
      }

      return results.toDS
    }

    def doGetAsIncomeLevelsDataset (spark : SparkSession) : Dataset[FlatPaginatedIdentityValueBean] = {

      val results = queryBuilder.doGetAsIncomeLevels()

      val resultList : Buffer[IdentityValueBean] = asScalaBuffer(results.getIncomeLevelList.asInstanceOf[java.util.List[IdentityValueBean]])

      return doGetAsFlatPaginatedIdentityValueBeanDataset (spark, results, resultList)
    }


    def doGetAsCatalogSourcesDataset (spark : SparkSession) : Dataset[FlatCatalogSources] = {

      import spark.implicits._

      val catalogSources = queryBuilder.doGetAsCatalogSources ()

      val results = MutableList[FlatCatalogSources]()

      results += new FlatCatalogSources ("catalogSources", catalogSources.getPage, catalogSources.getPages, catalogSources.getPerPage, catalogSources.getTotal, null, null, null, null, null)

      val catalogSourceList = catalogSources.getSourceList

      for (catalogSource <- catalogSourceList) {

        results += new FlatCatalogSources ("catalogSource", null, null, null, null, catalogSource.getId, catalogSource.getValue, catalogSource.getName, catalogSource.getDescription, catalogSource.getUrl)
      }

      return results.toDS
    }

    def doGetAsCountriesDataset (spark : SparkSession) : Dataset[FlatCountries] = {

      import spark.implicits._

      val countries = queryBuilder.doGetAsCountries ()

      val results = MutableList[FlatCountries]()

      var primaryKey : Long = 0

      results += new FlatCountries (primaryKey, "countries", countries.getPage, countries.getPages, countries.getPerPage, countries.getTotal, null, null, null, null, null, null, null)

      val countryList = countries.getCountryList

      for (country <- countryList) {

        results += new FlatCountries (primaryKey, "country", null, null, null, null, 
            null, null,
            country.getIsoCode, country.getName, country.getCapitalCity, country.getLongitude, country.getLatitude)

        results += new FlatCountries (primaryKey, "country.adminRegion", null, null, null, null, 
            country.getAdminRegion.getId, country.getAdminRegion.getValue,
            null, null, null, null, null)
//
        results += new FlatCountries (primaryKey, "country.incomeLevel", null, null, null, null, 
            country.getIncomeLevel.getId, country.getIncomeLevel.getValue,
            null, null, null, null, null)

        results += new FlatCountries (primaryKey, "country.lendingType", null, null, null, null, 
            country.getLendingType.getId, country.getLendingType.getValue,
            null, null, null, null, null)

        primaryKey += 1
      }

      return results.toDS()
    }
  }
}

trait PrimaryKey {
  def primaryKey : Long
}

trait FlatRowType {
  def rowType : String
}

trait FlatPaginationBean {
  def page : Integer
  def pages : Integer
  def perPage : Integer
  def total : Integer
}

trait FlatIdentityValueBean {
  def id : String
  def value : String
}

/**
 * These are all IdentityValueBeans
 * rowType = {region, adminRegion, incomeLevel, lendingType}
 */
case class FlatCountries (
  override val primaryKey : Long,
  override val rowType : String,
  override val page : Integer,
  override val pages : Integer,
  override val perPage : Integer,
  override val total : Integer,
  override val id : String,
  override val value : String,
  isoCode : String,
  name : String,
  capitalCity : String,
  longitude : String,
  latitude : String
) extends PrimaryKey with FlatPaginationBean with FlatRowType with FlatIdentityValueBean {}

case class FlatPaginatedIdentityValueBean (
    override val page : Integer,
    override val pages : Integer,
    override val perPage : Integer,
    override val total : Integer,
    override val id : String,
    override val value : String
) extends FlatPaginationBean with FlatIdentityValueBean {}

case class FlatDataPoints (
  dataPointDataPointIndicatorId : String,
  dataPointDataPointIndicatorValue : String,
  dataPointDataPointCountryId : String,
  dataPointDataPointCountryValue : String,
  dataPointValue : String,
  dataPointDecimal : String
) {}

case class FlatCatalogSources (
  override val rowType : String,
  override val page : Integer,
  override val pages : Integer,
  override val perPage : Integer,
  override val total : Integer,
  override val id : String,
  override val value : String,
  val name : String,
  val description : String,
  val url : String
) extends FlatPaginationBean with FlatRowType with FlatIdentityValueBean {}

/**
 * 
 */
case class FlatIndicators (
  indicatorsColumnType : String, // IndicatorsColumnType.Value
  indicatorName : String,
  indicatorSourceNote : String,
  indicatorSourceOrganization : String,
  indicatorId : String,
  indicatorSourceId : String,
  indicatorSourceValue : String,
  indicatorIndicatorTopicId : String,
  indicatorIndicatorTopicValue : String
) {}