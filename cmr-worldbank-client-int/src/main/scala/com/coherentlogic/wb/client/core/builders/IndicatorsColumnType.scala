package com.coherentlogic.wb.client.core.builders

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
object IndicatorsColumnType extends Enumeration {
  val source, indicatorTopic = Value
}