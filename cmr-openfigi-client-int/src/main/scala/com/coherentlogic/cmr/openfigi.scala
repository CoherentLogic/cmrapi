package com.coherentlogic.cmr

import com.coherentlogic.cmr.common.QueryBuilderDecorator

import scala.collection.mutable.MutableList

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.SparkSession

import com.coherentlogic.coherent.data.adapter.openfigi.client.core.builders.QueryBuilder
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.DataEntry
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.ErrorEntry
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean

import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.Data

import scala.collection.JavaConversions._

import com.jamonapi.Monitor
import com.jamonapi.MonitorFactory

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.coherentlogic.cmr._

import scala.collection.mutable.Seq

package object openfigi {

  implicit class OpenFIGIQueryBuilderDecorator (queryBuilder : QueryBuilder)
    extends QueryBuilderDecorator[Data] {

    private final val log = LoggerFactory.getLogger(classOf[OpenFIGIQueryBuilderDecorator])

/*    def doGetAsSeq[T] (
      data : Data,
      mapper : (com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.Data) => Seq[T]
    ) : Seq[T] = {

      val monitor = MonitorFactory.start(
        "com.coherentlogic.cmr.openfigi.OpenFIGIQueryBuilderDecorator.doGetAsSeq")

      // val data = queryBuilder.doGetAsData ()

      val mutableResult : Seq[T] = mapper (data)

      monitor.stop()

      val message = s"Method performance: $monitor"

      printAndLog (message)

      mutableResult
    }*/

    private def toFlatData (dataEntry : DataEntry) : FlatData = {

      new FlatData (
        dataEntry.getFigi (),
        dataEntry.getName (),
        dataEntry.getTicker (),
        dataEntry.getExchangeCode (),
        dataEntry.getCompositeFIGI (),
        dataEntry.getUniqueID (),
        dataEntry.getSecurityType (),
        dataEntry.getMarketSector (),
        dataEntry.getShareClassFIGI (),
        dataEntry.getUniqueIDForFutureOption (),
        null
      )
    }

    private def toFlatData (errorEntry : ErrorEntry) : FlatData = {

      new FlatData (
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        errorEntry.getError ()
      )
    }

    private def toFlatData (serializableBeans : java.util.List[SerializableBean]) : MutableList[FlatData] = {

      var result = MutableList[FlatData]()

      for (serializableBean <- serializableBeans) {

        if (serializableBean.isInstanceOf[DataEntry])
          result += toFlatData (serializableBean.asInstanceOf[DataEntry])
        else
          result += toFlatData (serializableBean.asInstanceOf[ErrorEntry])
      }

      return result
    }

    def doGetAsDataDataset (spark : SparkSession) : Dataset[FlatData] = {

      import spark.implicits._

      val monitor = MonitorFactory.start("openFIGIClient.doGetAsDataDataset")

      val flatDataList = MutableList[FlatData]()

      val data = queryBuilder.doGetAsData()

      val entries = data.getEntries ()

      for (entry <- entries)
        flatDataList ++= toFlatData (entry.asInstanceOf[java.util.List[SerializableBean]])

      monitor.stop

      val message = s"Method performance: $monitor"

      printAndLog (message)

      flatDataList.toDS
    }

    /**
     * Lazy solution to creating an empty dataset as FlatData is not public.
     */
    def emptyDataset (spark : SparkSession) : Dataset[FlatData] = {

      import spark.implicits._

      return spark.emptyDataset[FlatData]
    }
  }
}

case class FlatData (
    figi : String,
    name : String,
    ticker : String,
    exchangeCode : String,
    compositeFIGI : String,
    uniqueID : String,
    securityType : String,
    marketSector : String,
    shareClassFIGI : String,
    uniqueIDForFutureOption : String,
    error : String
) {}