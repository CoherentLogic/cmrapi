package com.coherentlogic.cmr

import com.coherentlogic.cmr.common.QueryBuilderDecorator
import com.coherentlogic.coherent.data.adapter.lei.client.core.builders.QueryBuilder
import scala.collection.mutable.MutableList
import scala.collection.Seq
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIResponse
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIEntry
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEI
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.Entity
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.Address
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.RegistrationAuthority
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.Dataset

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.jamonapi.Monitor
import com.jamonapi.MonitorFactory

import com.coherentlogic.cmr._

import scala.collection.JavaConversions._
import scala.compat.java8.FunctionConverters._

import java.sql.Date

package object lei {

  implicit class LEIQueryBuilderDecorator (queryBuilder : QueryBuilder)
    extends QueryBuilderDecorator[LEIResponse] {

    private final val log = LoggerFactory.getLogger(classOf[LEIQueryBuilderDecorator])

    private def doGetAsFlatLEIResponse () : MutableList[FlatLEIResponse] = {

      val response : LEIResponse = queryBuilder.doGetAsLEIResponse()

      val flatFlatLEIResponseList = MutableList[FlatLEIResponse] ()

      var ctr = 0

      response.forEachLEIEntry (
        asJavaConsumer[LEIEntry] (
          (leiEntry : LEIEntry) => {

            val lei : LEI = leiEntry.getLei

            val leiValue = lei.getValue

            flatFlatLEIResponseList += new FlatLEIResponse(
              s"[${ctr}]:leiEntry.lei",
              leiValue,
              null, null, // leiEntry.entity.legalName
              null, null, null, null, null, null, // leiEntity.getAddress
              null, null, // registrationAuthority
              null, null, // leiEntry.entity
              null, null, null, null, null, null // leiEntity.registration
            )

            val entityLegalName = leiEntry.getEntity.getLegalName.getValue

            val leiEntity : Entity = leiEntry.getEntity

            flatFlatLEIResponseList += new FlatLEIResponse(
              s"[${ctr}]:leiEntry.entity.legalName",
              null,
              leiEntity.getLegalName.getXMLLang,
              leiEntity.getLegalName.getValue,
              null, null, null, null, null, null, // leiEntity.getAddress
              null, null, // registrationAuthority
              null, null, // leiEntry.entity
              null, null, null, null, null, null // leiEntity.registration
            )

            flatFlatLEIResponseList += new FlatLEIResponse(
              s"[${ctr}]:leiEntry.entity.legalAddress",
              null,
              null,
              null,
              leiEntity.getLegalAddress.getType,
              leiEntity.getLegalAddress.getXMLLang,
              leiEntity.getLegalAddress.getFirstAddressLine,
              leiEntity.getLegalAddress.getCity,
              leiEntity.getLegalAddress.getCountry,
              leiEntity.getLegalAddress.getPostalCode,
              null, null, // registrationAuthority
              null, null, // leiEntry.entity
              null, null, null, null, null, null // leiEntity.registration
            )

            flatFlatLEIResponseList += new FlatLEIResponse(
              s"[${ctr}]:leiEntry.entity.headquartersAddress",
              null,
              null,
              null,
              leiEntity.getHeadquartersAddress.getType,
              leiEntity.getHeadquartersAddress.getXMLLang,
              leiEntity.getHeadquartersAddress.getFirstAddressLine,
              leiEntity.getHeadquartersAddress.getCity,
              leiEntity.getHeadquartersAddress.getCountry,
              leiEntity.getHeadquartersAddress.getPostalCode,
              null, null, // registrationAuthority
              null, null,  // leiEntry.entity
              null, null, null, null, null, null // leiEntity.registration
            )

            var otherAddressCtr = 0

            leiEntity.forEachOtherAddress (
              asJavaConsumer[Address] (
                (address : Address) => {

                flatFlatLEIResponseList += new FlatLEIResponse(
                  s"[${ctr}]:leiEntry.entity.otherAddress[${otherAddressCtr}]",
                  null,
                  null,
                  null,
                  address.getType,
                  address.getXMLLang,
                  address.getFirstAddressLine,
                  address.getCity,
                  address.getCountry,
                  address.getPostalCode,
                  null, null, // registrationAuthority
                  null, null,  // leiEntry.entity
                  null, null, null, null, null, null // leiEntity.registration
                )
                otherAddressCtr += 1
              }
            )
          )

          flatFlatLEIResponseList += new FlatLEIResponse(
            s"[${ctr}]:leiEntry.entity",
            null,
            null,
            null,
            null, null, null, null, null, null, // leiEntity.getAddress
            null, null, // registrationAuthority
            leiEntry.getEntity.getLegalJurisdiction,
            leiEntry.getEntity.getEntityStatus,
            null, null, null, null, null, null // leiEntity.registration
          )

          val leiEntryEntityRegistration = leiEntry.getEntity.getRegistration

          if (leiEntryEntityRegistration != null) {
            flatFlatLEIResponseList += new FlatLEIResponse(
              s"[${ctr}]:leiEntry.entity.registration",
              null,
              null,
              null,
              null, null, null, null, null, null, // leiEntity.getAddress
              null, null, // registrationAuthority
              null, // leiEntry.getEntity.getLegalJurisdiction,
              null, // leiEntry.getEntity.getEntityStatus,
              leiEntryEntityRegistration.getInitialRegistrationDateAsSQLDate (),
              leiEntryEntityRegistration.getLastUpdateDateAsSQLDate (),
              leiEntryEntityRegistration.getRegistrationStatus (),
              leiEntryEntityRegistration.getNextRenewalDateAsSQLDate (),
              leiEntryEntityRegistration.getManagingLOU (),
              leiEntryEntityRegistration.getValidationSources ()
            )
          }

          val leiEntryRegistration = leiEntry.getRegistration

          if (leiEntryRegistration != null) {
            flatFlatLEIResponseList += new FlatLEIResponse(
              s"[${ctr}]:leiEntry.registration",
              null,
              null,
              null,
              null, null, null, null, null, null, // leiEntity.getAddress
              null, null, // registrationAuthority
              null, // leiEntry.getEntity.getLegalJurisdiction,
              null, // leiEntry.getEntity.getEntityStatus,
              leiEntryRegistration.getInitialRegistrationDateAsSQLDate (),
              leiEntryRegistration.getLastUpdateDateAsSQLDate (),
              leiEntryRegistration.getRegistrationStatus (),
              leiEntryRegistration.getNextRenewalDateAsSQLDate (),
              leiEntryRegistration.getManagingLOU (),
              leiEntryRegistration.getValidationSources ()
            )
          }

          ctr += 1

        }
      )
    )

    flatFlatLEIResponseList
  }

  def doGetAsLEIResponseDataset (implicit spark : SparkSession) : Dataset[FlatLEIResponse] = {

      val monitor = MonitorFactory.start("doGetAsLEIResponseDataset")

      import spark.implicits._

      val result = doGetAsFlatLEIResponse ().toDS

      monitor.stop

      val message = s"Method performance: $monitor"

      printAndLog (message)

      result
    }
  }
}

case class FlatLEIResponse (
  kind : String,
  leiValue : String,
  entityLegalNameXMLLang : String,
  entityLegalNameValue : String,
  entityAddressType : String,
  entityAddressXMLLang : String,
  entityAddressFirstAddressLine : String,
  entityAddressCity : String,
  entityAddressCountry : String,
  entityAddressPostalCode : String,
  registrationAuthorityID : String,
  registrationAuthorityEntityID : String,
  entityLegalJurisdiction : String,
  entityEntityStatus : String,
  registrationInitialRegistrationDate : Date,
  registrationLastUpdateDate : Date,
  registrationRegistrationStatus : String,
  registrationNextRenewalDate : Date,
  registrationManagingLOU : String,
  registrationValidationSources : String
) {}
