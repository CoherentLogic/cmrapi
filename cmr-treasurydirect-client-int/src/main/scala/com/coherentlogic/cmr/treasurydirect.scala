package com.coherentlogic.cmr

import com.coherentlogic.cmr.common.QueryBuilderDecorator
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean

import java.sql.Date
import java.text.SimpleDateFormat

import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.mutable.MutableList

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.SparkSession

import com.jamonapi.Monitor
import com.jamonapi.MonitorFactory

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
import scala.collection.mutable.MutableList
import spark.implicits._
case class FlatSecurities (cusip : String,interestRate : Option[BigDecimal], someDate : Option[java.sql.Date]){}
val result = MutableList[FlatSecurities]()
val someTime : java.lang.Long = null
val fs = new FlatSecurities ("blah", null, Some (null)) //new java.sql.Date (someTime)))
result += fs
result.toDS ().show ()



 * 
 */

import com.coherentlogic.treasurydirect.client.core.builders.QueryBuilder

import com.coherentlogic.cmr._

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean

package object treasurydirect {

    implicit class TreasuryDirectQueryBuilderDecorator  (queryBuilder : QueryBuilder)
      extends QueryBuilderDecorator[SerializableBean] {

      private final val log = LoggerFactory.getLogger(classOf[TreasuryDirectQueryBuilderDecorator])

      def doGetAsDebtsDataset (spark : SparkSession) : Dataset[FlatDebts] = {

        import spark.implicits._

        val monitor = MonitorFactory.start("doGetAsDebtsDataset")

        var result = MutableList[FlatDebts]()

        val debts = queryBuilder.doGetAsDebts()

        val debtList = debts.getDebtList ()

        for (debt <- debtList) {
          result += new FlatDebts (
            Option(toSQLDate (debt.getEffectiveDate ())),
            Option(debt.getGovernmentHoldings ()),
            Option(debt.getPublicDebt ()),
            Option(debt.getTotalDebt ())
          )
        }

        monitor.stop

        val message = s"Method performance: $monitor"

        printAndLog (message)

        return result.toDS
      }

      def doGetAsSecuritiesDataset (spark : SparkSession) : Dataset[FlatSecurities] = {

        import spark.implicits._

        val monitor = MonitorFactory.start("doGetAsSecuritiesDataset")

        var result = MutableList[FlatSecurities]()

        val securities = queryBuilder.doGetAsSecurities()

        val securityList = securities.getSecurityList ()

        for (security <- securityList) {

          val flatSecurities = new FlatSecurities (
              Option(security.getCusip ()),
              Option(toSQLDate (security.getIssueDate ())),
              Option(security.getSecurityType ()),
              Option(security.getSecurityTerm ()),
              Option(toSQLDate (security.getMaturityDate ())),
              if (security.getInterestRate () == null) { null } else { Option(new scala.math.BigDecimal( security.getInterestRate ())) },
              if (security.getRefCpiOnIssueDate () == null) { null } else { Option(new scala.math.BigDecimal( security.getRefCpiOnIssueDate ())) },
              if (security.getRefCpiOnDatedDate () == null) { null } else { Option(new scala.math.BigDecimal( security.getRefCpiOnDatedDate ())) },
              Option(toSQLDate (security.getAnnouncementDate ())),
              Option(toSQLDate (security.getAuctionDate ())),
              Option(security.getAuctionDateYear ()),
              Option(toSQLDate (security.getDatedDate ())),
              if (security.getAccruedInterestPer1000 () == null) { null } else { Option(new scala.math.BigDecimal( security.getAccruedInterestPer1000 ())) },
              if (security.getAccruedInterestPer100 () == null) { null } else { Option(new scala.math.BigDecimal( security.getAccruedInterestPer100 ())) },
              if (security.getAdjustedAccruedInterestPer1000 () == null) { null } else { Option(new scala.math.BigDecimal( security.getAdjustedAccruedInterestPer1000 ())) },
              if (security.getAdjustedPrice () == null) { null } else { Option(new scala.math.BigDecimal( security.getAdjustedPrice ())) },
              if (security.getAllocationPercentage () == null) { null } else { Option(new scala.math.BigDecimal( security.getAllocationPercentage ())) },
              Option(security.getAllocationPercentageDecimals ()),
              Option(security.getAnnouncedCusip ()),
              Option(security.getAuctionFormat ()),
              if (security.getAverageMedianDiscountRate () == null) { null } else { Option(new scala.math.BigDecimal( security.getAverageMedianDiscountRate ())) },
              if (security.getAverageMedianInvestmentRate () == null) { null } else { Option(new scala.math.BigDecimal( security.getAverageMedianInvestmentRate ())) },
              if (security.getAverageMedianPrice () == null) { null } else { Option(new scala.math.BigDecimal( security.getAverageMedianPrice ())) },
              if (security.getAverageMedianDiscountMargin () == null) { null } else { Option(new scala.math.BigDecimal( security.getAverageMedianDiscountMargin ())) },
              if (security.getAverageMedianYield () == null) { null } else { Option(new scala.math.BigDecimal( security.getAverageMedianYield ())) },
              Option(security.getBackDated ()),
              Option(toSQLDate (security.getBackDatedDate ())),
              if (security.getBidToCoverRatio () == null) { null } else { Option(new scala.math.BigDecimal( security.getBidToCoverRatio ())) },
              Option(security.getCallDate ()),
              Option(security.getCallable ()),
              Option(security.getCalledDate ()),
              Option(security.getCashManagementBillCMB ()),
              Option(toSQLDate (security.getClosingTimeCompetitive ())),
              Option(toSQLDate (security.getClosingTimeNoncompetitive ())),
              Option(security.getCompetitiveAccepted ()),
              Option(security.getCompetitiveBidDecimals ()),
              Option(security.getCompetitiveTendered ()),
              Option(security.getCompetitiveTendersAccepted ()),
              Option(security.getCorpusCusip ()),
              Option(security.getCpiBaseReferencePeriod ()),
              Option(security.getCurrentlyOutstanding ()),
              Option(security.getDirectBidderAccepted ()),
              Option(security.getDirectBidderTendered ()),
              Option(security.getEstimatedAmountOfPubliclyHeldMaturingSecuritiesByType ()),
              Option(security.getFimaIncluded ()),
              if (security.getFimaNoncompetitiveAccepted () == null) { null } else { Option(new scala.math.BigDecimal( security.getFimaNoncompetitiveAccepted ())) },
              if (security.getFimaNoncompetitiveTendered () == null) { null } else { Option(new scala.math.BigDecimal( security.getFimaNoncompetitiveTendered ())) },
              Option(security.getFirstInterestPeriod ()),
              Option (toSQLDate (security.getFirstInterestPaymentDate ())),
              Option(security.getFloatingRate ()),
              Option (toSQLDate (security.getFrnIndexDeterminationDate ())),
              if (security.getFrnIndexDeterminationRate () == null) { null } else { Option(new scala.math.BigDecimal( security.getFrnIndexDeterminationRate ())) },
              if (security.getHighDiscountRate () == null) { null } else { Option(new scala.math.BigDecimal( security.getHighDiscountRate ())) },
              if (security.getHighInvestmentRate () == null) { null } else { Option(new scala.math.BigDecimal( security.getHighInvestmentRate ())) },
              Option(security.getHighPrice ()),
              if (security.getHighDiscountMargin () == null) { null } else { Option(new scala.math.BigDecimal( security.getHighDiscountMargin ())) },
              if (security.getHighYield () == null) { null } else { Option(new scala.math.BigDecimal( security.getHighYield ())) },
              if (security.getIndexRatioOnIssueDate () == null) { null } else { Option(new scala.math.BigDecimal( security.getIndexRatioOnIssueDate ())) },
              Option(security.getIndirectBidderAccepted ()),
              Option(security.getIndirectBidderTendered ()),
              Option(security.getInterestPaymentFrequency ()),
              if (security.getLowDiscountRate () == null) { null } else { Option(new scala.math.BigDecimal( security.getLowDiscountRate ())) },
              if (security.getLowInvestmentRate () == null) { null } else { Option(new scala.math.BigDecimal( security.getLowInvestmentRate ())) },
              if (security.getLowPrice () == null) { null } else { Option(new scala.math.BigDecimal( security.getLowPrice ())) },
              if (security.getLowDiscountMargin () == null) { null } else { Option(new scala.math.BigDecimal( security.getLowDiscountMargin ())) },
              if (security.getLowYield () == null) { null } else { Option(new scala.math.BigDecimal( security.getLowYield ())) },
              Option (toSQLDate (security.getMaturingDate ())),
              if (security.getMaximumCompetitiveAward () == null) { null } else { Option(new scala.math.BigDecimal( security.getMaximumCompetitiveAward ())) },
              if (security.getMaximumNoncompetitiveAward () == null) { null } else { Option(new scala.math.BigDecimal( security.getMaximumNoncompetitiveAward ())) },
              if (security.getMaximumSingleBid () == null) { null } else { Option(new scala.math.BigDecimal( security.getMaximumSingleBid ())) },
              if (security.getMinimumBidAmount () == null) { null } else { Option(new scala.math.BigDecimal( security.getMinimumBidAmount ())) }, //
              Option(security.getMinimumStripAmount ()),
              Option(security.getMinimumToIssue ()),
              Option(security.getMultiplesToBid ()),
              Option(security.getMultiplesToIssue ()),
              Option(security.getNlpExclusionAmount ()),
              Option(security.getNlpReportingThreshold ()), //
              Option(security.getNoncompetitiveAccepted ()),
              Option(security.getNoncompetitiveTendersAccepted ()),
              Option(security.getOfferingAmount ()),
              Option(security.getOriginalCusip ()),
              Option(toSQLDate (security.getOriginalDatedDate ())),
              Option(toSQLDate (security.getOriginalIssueDate ())),
              Option(security.getOriginalSecurityTerm ()),
              Option(security.getPdfFilenameAnnouncement ()),
              Option(security.getPdfFilenameCompetitiveResults ()),
              Option(security.getPdfFilenameNoncompetitiveResults ()),
              Option(security.getPdfFilenameSpecialAnnouncement ()),
              if (security.getPricePer100 () == null) { null } else { Option(new scala.math.BigDecimal( security.getPricePer100 ())) },
              Option(security.getPrimaryDealerAccepted ()),
              Option(security.getPrimaryDealerTendered ()),
              Option(security.getReopening ()),
              Option(security.getSecurityTermDayMonth ()),
              Option(security.getSecurityTermWeekYear ()),
              Option(security.getSeries ()),
              Option(security.getSomaAccepted ()),
              Option(security.getSomaHoldings ()),
              Option(security.getSomaIncluded ()),
              Option(security.getSomaTendered ()),
              if (security.getSpread () == null) { null } else { Option(new scala.math.BigDecimal( security.getSpread ())) },
              if (security.getStandardInterestPaymentPer1000 () == null) { null } else { Option(new scala.math.BigDecimal( security.getStandardInterestPaymentPer1000 ())) },
              Option(security.getStrippable ()),
              Option(security.getTerm ()),
              if (security.getTiinConversionFactorPer1000 () == null) { null } else { Option(new scala.math.BigDecimal( security.getTiinConversionFactorPer1000 ())) },
              Option(security.getTips ()),
              Option(security.getTotalAccepted ()),
              Option(security.getTotalTendered ()),
              Option(security.getTreasuryDirectAccepted ()),
              Option(security.getTreasuryDirectTendersAccepted ()),
              Option(security.getType ()),
              if (security.getUnadjustedAccruedInterestPer1000 () == null) { null } else { Option(new scala.math.BigDecimal( security.getUnadjustedAccruedInterestPer1000 ())) },
              if (security.getUnadjustedPrice () == null) { null } else { Option(new scala.math.BigDecimal( security.getUnadjustedPrice ())) },
              Option(toSQLDate (security.getUpdatedTimestamp ())),
              Option(security.getXmlFilenameAnnouncement ()),
              Option(security.getXmlFilenameCompetitiveResults ()),
              Option(security.getXmlFilenameSpecialAnnouncement ())
          )

          result += flatSecurities
        }

        monitor.stop

        val message = s"Method performance: $monitor"

        printAndLog (message)

        return result.toDS
      }

      private def toSQLDate (originalDate : java.util.Date) : Date = {
        return if (originalDate == null) { null } else { new Date (originalDate.getTime()) }
      }

      private def toSQLDate (originalDate : String) : Date = {

        val simpleDateFormat : SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        val date : java.util.Date = simpleDateFormat.parse(originalDate);

        return new Date (date.getTime)
      }
    }
}

case class FlatDebts (effectiveDate : Option[Date], governmentHoldings : Option[BigDecimal], publicDebt : Option[BigDecimal], totalDebt : Option[BigDecimal]) {}

case class FlatSecurities (
    cusip : Option[String],
    issueDate : Option[Date],
    securityType : Option[String],
    securityTerm : Option[String],
    maturityDate : Option[Date],
    interestRate : Option[BigDecimal],
    refCpiOnIssueDate : Option[BigDecimal],
    refCpiOnDatedDate : Option[BigDecimal],
    announcementDate : Option[Date],
    auctionDate : Option[Date],
    auctionDateYear : Option[String],
    datedDate : Option[Date],
    accruedInterestPer1000 : Option[BigDecimal],
    accruedInterestPer100 : Option[BigDecimal],
    adjustedAccruedInterestPer1000 : Option[BigDecimal],
    adjustedPrice : Option[BigDecimal],
    allocationPercentage : Option[BigDecimal],
    allocationPercentageDecimals : Option[Integer],
    announcedCusip : Option[String],
    auctionFormat : Option[String],
    averageMedianDiscountRate : Option[BigDecimal],
    averageMedianInvestmentRate : Option[BigDecimal],
    averageMedianPrice : Option[BigDecimal],
    averageMedianDiscountMargin : Option[BigDecimal],
    averageMedianYield : Option[BigDecimal],
    backDated : Option[Boolean],
    backDatedDate : Option[Date],
    bidToCoverRatio : Option[BigDecimal],
    callDate : Option[String],
    callable : Option[Boolean],
    calledDate : Option[String],
    cashManagementBillCMB : Option[Boolean],
    closingTimeCompetitive : Option[Date],
    closingTimeNoncompetitive : Option[Date],
    competitiveAccepted : Option[Long],
    competitiveBidDecimals : Option[Integer],
    competitiveTendered : Option[Long],
    competitiveTendersAccepted : Option[Boolean],
    corpusCusip : Option[String],
    cpiBaseReferencePeriod : Option[String],
    currentlyOutstanding : Option[String],
    directBidderAccepted : Option[Long],
    directBidderTendered : Option[Long],
    estimatedAmountOfPubliclyHeldMaturingSecuritiesByType : Option[String],
    fimaIncluded : Option[Boolean],
    fimaNoncompetitiveAccepted : Option[BigDecimal],
    fimaNoncompetitiveTendered : Option[BigDecimal],
    firstInterestPeriod : Option[String],
    firstInterestPaymentDate : Option[Date],
    floatingRate : Option[String],
    frnIndexDeterminationDate : Option[Date],
    frnIndexDeterminationRate : Option[BigDecimal],
    highDiscountRate : Option[BigDecimal],
    highInvestmentRate : Option[BigDecimal],
    highPrice : Option[String],
    highDiscountMargin : Option[BigDecimal],
    highYield : Option[BigDecimal],
    indexRatioOnIssueDate : Option[BigDecimal],
    indirectBidderAccepted : Option[Long],
    indirectBidderTendered : Option[Long],
    interestPaymentFrequency : Option[String],
    lowDiscountRate : Option[BigDecimal],
    lowInvestmentRate : Option[BigDecimal],
    lowPrice : Option[BigDecimal],
    lowDiscountMargin : Option[BigDecimal],
    lowYield : Option[BigDecimal],
    maturingDate : Option[Date],
    maximumCompetitiveAward : Option[BigDecimal],
    maximumNoncompetitiveAward : Option[BigDecimal],
    maximumSingleBid : Option[BigDecimal],
    minimumBidAmount : Option[BigDecimal],//
    minimumStripAmount : Option[Integer],
    minimumToIssue : Option[Integer],
    multiplesToBid : Option[Integer],
    multiplesToIssue : Option[Integer],
    nlpExclusionAmount : Option[Long],
    nlpReportingThreshold : Option[String],
    noncompetitiveAccepted : Option[Long],
    noncompetitiveTendersAccepted : Option[Boolean],
    offeringAmount : Option[Long],
    originalCusip : Option[String],
    originalDatedDate : Option[Date],
    originalIssueDate : Option[Date],
    originalSecurityTerm : Option[String],
    pdfFilenameAnnouncement : Option[String],
    pdfFilenameCompetitiveResults : Option[String],
    pdfFilenameNoncompetitiveResults : Option[String],
    pdfFilenameSpecialAnnouncement : Option[String],
    pricePer100 : Option[BigDecimal],
    primaryDealerAccepted : Option[Long],
    primaryDealerTendered : Option[Long],
    reopening : Option[Boolean],
    securityTermDayMonth : Option[String],
    securityTermWeekYear : Option[String],
    series : Option[String],
    somaAccepted : Option[Long],
    somaHoldings : Option[Long],
    somaIncluded : Option[Boolean],
    somaTendered : Option[Long],
    spread : Option[BigDecimal],
    standardInterestPaymentPer1000 : Option[BigDecimal],
    strippable : Option[Boolean],
    term : Option[String],
    tiinConversionFactorPer1000 : Option[BigDecimal],
    tips : Option[Boolean],
    totalAccepted : Option[Long],
    totalTendered : Option[Long],
    treasuryDirectAccepted : Option[Long],
    treasuryDirectTendersAccepted : Option[Boolean],
    typeOf : Option[String],
    unadjustedAccruedInterestPer1000 : Option[BigDecimal],
    unadjustedPrice : Option[BigDecimal],
    updatedTimestamp : Option[Date],
    xmlFilenameAnnouncement : Option[String],
    xmlFilenameCompetitiveResults : Option[String],
    xmlFilenameSpecialAnnouncement : Option[String]
) {
}