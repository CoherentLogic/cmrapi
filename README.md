# CMRAPI

## Download

Find the jar file download link [here](https://coherentlogic.com/wordpress/cmr/).

## Run the examples in Spark Shell as follows

/.../spark-2.3.0-bin-hadoop2.7/bin>spark-shell --jars cmr-api-assembly-2.0.2.1-RELEASE.jar

A key for the FRED API can be acquired here:
  https://research.stlouisfed.org/docs/api/api_key.html

A key for the OpenFIGI API can be acquired here:
  https://openfigi.com/api

## Man

If you're running Spark on a desktop with a UI, executing

```scala
cmr.man
```

will open the default browser and go to the CMR documentation page.

## Demo (2.0.3-RELEASE)

/Users/.../development/software/spark-2.3.0-bin-hadoop2.7/bin

C:\development\software\spark-2.3.0-bin-hadoop2.7\bin>spark-shell --packages "com.coherentlogic.cmr.api:cmr-api-core:2.0.3-RELEASE,dom4j:dom4j:1.6.1,com.fasterxml.jackson.core:jackson-databind:2.9.6" --exclude-packages "junit:junit,org.jboss.spec.javax.ws.rs:jboss-jaxrs-api_2.1_spec,org.jboss.spec.javax.servlet:jboss-servlet-api_3.1_spec,org.jboss.spec.javax.annotation:jboss-annotations-api_1.2_spec,org.jboss.logging:jboss-logging-annotations,org.jboss.logging:jboss-logging-processor,org.jboss.spec.javax.xml.bind:jboss-jaxb-api_2.3_spec,org.reactivestreams:reactive-streams,javax.activation:activation,net.jcip:jcip-annotations,javax.validation:validation-api,javax.json.bind:javax.json.bind-api,javax.ws.rs:javax.ws.rs-api"

bash$ ./spark-shell --packages "com.coherentlogic.cmr.api:cmr-api-core:2.0.3-RELEASE,dom4j:dom4j:1.6.1,com.fasterxml.jackson.core:jackson-databind:2.9.6" --exclude-packages "junit:junit,org.jboss.spec.javax.ws.rs:jboss-jaxrs-api_2.1_spec,org.jboss.spec.javax.servlet:jboss-servlet-api_3.1_spec,org.jboss.spec.javax.annotation:jboss-annotations-api_1.2_spec,org.jboss.logging:jboss-logging-annotations,org.jboss.logging:jboss-logging-processor,org.jboss.spec.javax.xml.bind:jboss-jaxb-api_2.3_spec,org.reactivestreams:reactive-streams,javax.activation:activation,net.jcip:jcip-annotations,javax.validation:validation-api,javax.json.bind:javax.json.bind-api,javax.ws.rs:javax.ws.rs-api"

import com.coherentlogic.cmr.lei._
import com.coherentlogic.cmr.api.builders.CMR
val cmr = new CMR ()
val leiResult = cmr.lei.leirecords().withLEI("35380063V116GJUGS786").doGetAsLEIResponseDataset(spark)
leiResult.show()

cmr.lei.path[com.coherentlogic.coherent.data.adapter.lei.client.core.builders.QueryBuilder] ("foo").getEscapedURI


import com.coherentlogic.cmr.fred._
import com.coherentlogic.cmr.api.builders.CMR
val cmr = new CMR ()

cmr.fred.path[com.coherentlogic.fred.client.core.builders.QueryBuilder] ("releases").replaceQuery[com.coherentlogic.fred.client.core.builders.QueryBuilder] ("api_key=[redacted]").doGetAsReleases()

cmr.fred.path[com.coherentlogic.fred.client.core.builders.QueryBuilder] ("fred/releases").replaceQuery[com.coherentlogic.fred.client.core.builders.QueryBuilder] ("api_key=[redacted]").doGetAsReleasesDataset (spark)
cmr.fred.replaceQuery[com.coherentlogic.fred.client.core.builders.QueryBuilder] ("release_id=51&realtime_start=2001-01-20&realtime_end=2004-05-17").withExternalApiKey ("FRED_API_KEY").doGetAsSeriessDataset(spark)
https://api.stlouisfed.org/fred/releases?api_key=[redacted]

import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIResponse
import org.apache.spark.sql.Encoder
import org.apache.spark.sql.Encoders
import com.coherentlogic.cmr.lei._
import com.coherentlogic.cmr.api.builders.CMR
val cmr = new CMR ()
val leiResponse = cmr.lei.leirecords().withLEI("35380063V116GJUGS786").doGetAsLEIResponse
implicit val leiResponseEncoder : Encoder[LEIResponse] = Encoders.kryo [com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIResponse]
spark.createDataset (Seq (leiResponse))


val leiResponseSeq = Seq (leiResult)

import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEI
val leiEncoder = Encoders.kryo [com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEI]

import com.coherentlogic.cmr.openfigi._
import com.coherentlogic.cmr.api.builders.CMR
val cmr = new CMR ()
val dataDS = cmr.openFIGI.getRequestBody.withFinancialInstrumentGlobalIdentifier("BBG00LG7F9S4").done.doGetAsDataDataset(spark)
dataDS.show

cmr.treasuryDirect.taWs.securities.auctioned.withTypeAsTIPS().withDays(720).getEscapedURI
cmr.treasuryDirect.taWs.securities.auctioned.withTypeAsTIPS().withDays(720).doGetAsString

import com.coherentlogic.cmr.api.builders.CMR
val cmr = new CMR ()
import com.coherentlogic.cmr.fred._
val FRED_API_KEY = System.getenv ("FRED_API_KEY")
val observationsDS = cmr.fred.series.observations.withExternalApiKey (FRED_API_KEY).withSeriesId("SP500").doGetAsObservationsDataset(spark)
observationsDS.show ()

val observationsDS1 = cmr.fred.series.observations.withExternalApiKey (FRED_API_KEY).withSeriesId("SP500").doGetAsObservationsDataset(spark)

import com.coherentlogic.cmr.treasurydirect._
val debts = cmr.treasuryDirect.debt().current().doGetAsDebtsDataset(spark)
debts.show
val securities = cmr.treasuryDirect.taWs.securities.auctioned.withTypeAsTIPS().withDays(720).doGetAsSecuritiesDataset(spark)
securities.show

import com.coherentlogic.cmr.wb._
val dataPointsDataset = cmr.worldBank.countries("br", "gb").indicators("SP.POP.TOTL").withDate("1998:2003").doGetAsDataPointsDataset(spark)


## Demo (2.0.2.1-RELEASE)

Here's a short demo -- note the FRED API key is required whereas the OpenFIGI
key is not.

```scala
val FRED_API_KEY = // Required: obtain yours here: https://research.stlouisfed.org/docs/api/api_key.html
val OPEN_FIGI_API_KEY = // Optional however limits are lower without one: https://openfigi.com/api
import com.coherentlogic.wb.client.core.builders.BuilderDecorators._
import com.coherentlogic.fred.client.core.builders.BuilderDecorators._
import com.coherentlogic.openfigi.client.core.builders.BuilderDecorators._
import com.coherentlogic.cmr.api.decorators.SparkSessionDecorators._
import com.coherentlogic.treasurydirect.client.core.builders.BuilderDecorators._
import com.coherentlogic.cmr.api.builders.CMR
val cmr = new CMR ()
val dataDS = spark.cmr.openFIGI.getRequestBody.withFinancialInstrumentGlobalIdentifier("BBG00K62SK81").done.doGetAsDataDataset(spark)
val debts = cmr.treasuryDirect.debt().current().doGetAsDebtsDataset(spark)
val securities = cmr.treasuryDirect.taWs.securities.auctioned.withTypeAsTIPS().withDays(720).doGetAsSecuritiesDataset(spark)
val dataPointsDataset = cmr.worldBank.countries("br", "gb").indicators("SP.POP.TOTL").withDate("1998:2003").doGetAsDataPointsDataset(spark)
val observationsDS = cmr.fred.series.observations.withApiKey (FRED_API_KEY).withSeriesId("SP500").doGetAsObservationsDataset(spark)
```

## See Also

- [FRED Client](https://coherentlogic.com/wordpress/middleware-development/are-you-looking-to-retrieve-economic-data-from-the-federal-reserve-bank-of-st-louis/)
- [Coherent Data Adapter: US Treasury Direct Client](https://coherentlogic.com/wordpress/middleware-development/us-treasury-direct-client/)
- [World Bank Client](https://coherentlogic.com/wordpress/middleware-development/world-bank-client/)
- [Coherent Data Adapter: OpenFIGI Client Edition](https://coherentlogic.com/wordpress/middleware-development/coherent-data-adapter-openfigi-edition/)
- [Coherent Data Adapter: Quandl Client Edition](https://coherentlogic.com/wordpress/middleware-development/quandl-client/)
- [USA Spending Client](https://coherentlogic.com/wordpress/middleware-development/usa-spending-client/)
- [Coherent Datafeed: Thomson Reuters Edition](https://coherentlogic.com/wordpress/middleware-development/solutions-for-traders-using-thomson-reuters-market-data/)
- [Coherent Data Adapter: CUSIP Global Services Web Edition](https://coherentlogic.com/wordpress/middleware-development/coherent-data-adapter-cusip-global-services-web-edition/)
- [CMR: Rapid Data Acquisition for the Apache Spark Cluster Computing Platform](https://coherentlogic.com/wordpress/middleware-development/cmr/)
- [Coherent Logic Enterprise Data Adapter](https://coherentlogic.com/wordpress/middleware-development/enterprise-data-adapter/)

## Other example code appears below

```scala
val observationsDS = cmr.fred.series.observations.withApiKey ("[redacted]").withSeriesId("SP500").doGetAsObservationsDataset(spark)
observationsDS.count ()
observationsDS.show ()
val categoriesDS = cmr.fred.category().withApiKey (key).withCategoryId(125).doGetAsCategoriesDataset(spark)

import com.coherentlogic.fred.client.core.builders.BuilderDecorators._
import com.coherentlogic.openfigi.client.core.builders.BuilderDecorators._
import com.coherentlogic.treasurydirect.client.core.builders._
import com.coherentlogic.cmr.api.decorators.SparkSessionDecorators._
val dataDS = spark.cmr.openFIGI.getRequestBody.withFinancialInstrumentGlobalIdentifier("BBG00K62SK81").done.doGetAsDataDataset(spark)
dataDS.show ()

import com.coherentlogic.fred.client.core.builders.BuilderDecorators._
import com.coherentlogic.openfigi.client.core.builders.BuilderDecorators._
import com.coherentlogic.cmr.api.builders.CMR
val cmr = new CMR ()
val openFIGIExDS = cmr.openFIGI.getRequestBody.withFinancialInstrumentGlobalIdentifier("BBG00K62SK81").done.doGetAsDataDataset(spark)
openFIGIExDS.count ()
openFIGIExDS.show ()

import com.coherentlogic.openfigi.client.core.builders.BuilderDecorators._
import com.coherentlogic.cmr.api.builders.CMR
val cmr = new CMR ()
val data = cmr.openFIGI
  .getRequestBody()
    .newMappingEntry()
      .withIdType("ID_WERTPAPIER")
      .withIdValue("851399")
      .withExchangeCode("US")
    .done()
  .done()
.doGetAsDataDataset(spark)

data.show()

// assigned to the Canadian Securities Exchange listing of the Phivida Holdings Inc - warrants
import com.coherentlogic.fred.client.core.builders.BuilderDecorators._
import com.coherentlogic.openfigi.client.core.builders.BuilderDecorators._
import com.coherentlogic.cmr.api.builders.CMR
val cmr = new CMR ()
val openFIGIExDS = cmr.openFIGI.getRequestBody.withFinancialInstrumentGlobalIdentifier("BBG00KP7PCH0").done.doGetAsDataDataset(spark)
openFIGIExDS.count ()
openFIGIExDS.show ()

val OPEN_FIGI_API_KEY = System.getenv("OPEN_FIGI_API_KEY")
import com.coherentlogic.fred.client.core.builders.BuilderDecorators._
import com.coherentlogic.openfigi.client.core.builders.BuilderDecorators._
import com.coherentlogic.cmr.api.builders.CMR
val cmr = new CMR ()
val openFIGIExDS = cmr.openFIGI.withApiKey (OPEN_FIGI_API_KEY).getRequestBody.withCompositeFinancialInstrumentGlobalIdentifier(
"BBG000DFMR98",
"BBG000PH6QW5",
"BBG000PN1B23",
"BBG000Q3V483",
"BBG001QVHGY9",
"BBG00B0QVR58",
"BBG000BBNJJ5",
"BBG000BC20D3",
"BBG000BC8T71",
"BBG000BCJCF7",
"BBG000BDX7T5",
"BBG000BFPY74",
"BBG000BH9YB3",
"BBG000BHH9P4",
"BBG000BJ8YJ2",
"BBG000BLH038",
"BBG000BM0T68",
"BBG000BNWJ09",
"BBG000BQNJ93",
"BBG000BQRVZ3",
"BBG000BRRQX4",
"BBG000BV6341",
"BBG000BW2KX3",
"BBG000BXVPP7",
"BBG000BYCZP4",
"BBG000BZ7DN8",
"BBG000C02WX6",
"BBG000C0FNW3",
"BBG000C1W506",
"BBG000C2XR14",
"BBG000C43N30",
"BBG000C7VPF4",
"BBG000CB9Z95",
"BBG000CHGNC7",
"BBG000CHHZZ4",
"BBG000CP91H0",
"BBG000CS4KT4",
"BBG000D35PX1",
"BBG000D7H0D6",
"BBG000DGFHZ7",
"BBG000DK8D49",
"BBG000DSMFG8",
"BBG000DW1KC5",
"BBG000FKHH11",
"BBG000FR0C15",
"BBG000FVBS82",
"BBG000FWY148",
"BBG000G49218",
"BBG000GPGKW9",
"BBG000GQYB75",
"BBG000GRWLP3",
"BBG000GY2219",
"BBG000H21MJ6",
"BBG000H21RL2",
"BBG000H7K4W9",
"BBG000HJHCG7",
"BBG000J1BQN1",
"BBG000J22074",
"BBG000JL1DX6",
"BBG000JLH3S7",
"BBG000JPRBH1",
"BBG000JSX153",
"BBG000K3ZRN7",
"BBG000K48NZ1",
"BBG000KB54C5",
"BBG000KK6746",
"BBG000KKZZW1",
"BBG000KXS7T7",
"BBG000KZPN01",
"BBG000L190Y6",
"BBG000L9VSZ2",
"BBG000LJRMP9",
"BBG000LL94K9",
"BBG000LQ8WR0",
"BBG000LQSP75",
"BBG000LSR8P0",
"BBG000M0RRD1",
"BBG000M1HRD0",
"BBG000M5SW48",
"BBG000M9L2F1",
"BBG000N864S1",
"BBG000NBKGB9",
"BBG000NHK5F6",
"BBG000NLS587",
"BBG000NMBQ78",
"BBG000NQCXG3",
"BBG000NS3ZY4",
"BBG000P78VN4",
"BBG000P8NYC1",
"BBG000PDQPY2",
"BBG000PLT8C3",
"BBG000PMGKH3",
"BBG000PMLJW3",
"BBG000PRGH16",
"BBG000PRMW98",
"BBG000PT2XP6",
"BBG000PWR8N9",
"BBG000PXCBG4",
"BBG000PXW897"
).done.doGetAsDataDataset(spark)
openFIGIExDS.count ()
openFIGIExDS.show ()

import com.coherentlogic.fred.client.core.builders.BuilderDecorators._
import com.coherentlogic.openfigi.client.core.builders.BuilderDecorators._
import com.coherentlogic.cmr.api.builders.CMR
val cmr = new CMR ()
val openFIGIExDS = cmr.openFIGI.getRequestBody.withFinancialInstrumentGlobalIdentifier("BBG00KP7PCH0").done.doGetAsDataDataset(spark)
openFIGIExDS.count ()
openFIGIExDS.show ()

val openFIGIExDS = cmr.openFIGI.getRequestBody.withFinancialInstrumentGlobalIdentifier("BBG00KP7PCH0").withFinancialInstrumentGlobalIdentifier("BBG000BLNNV0").done.doGetAsDataDataset(spark)
val openFIGIResult = cmr.openFIGI.getRequestBody().newMappingEntry().withIdType("ID_ISIN").withIdValue("US4592001014").done().newMappingEntry().withIdType("ID_WERTPAPIER").withIdValue("851399").done().done().doGetAsDataDataset(spark)

val key = System.getenv("FRED_API_KEY")
import com.coherentlogic.fred.client.core.builders.BuilderDecorators._
import com.coherentlogic.openfigi.client.core.builders.BuilderDecorators._
import com.coherentlogic.cmr.api.builders.CMR
val cmr = new CMR ()
val sp500CaseShillerUSNationalHomePriceIndexDS = cmr.fred.series.observations.withApiKey (key).withSeriesId("CSUSHPINSA").doGetAsObservationsDataset(spark)
val sp500ObservationsDS = cmr.fred.series.observations.withApiKey (key).withSeriesId("SP500").doGetAsObservationsDataset(spark)
val sp500ObservationsDS = cmr.fred.series.observations.withSeriesId("SP500").doGetAsObservationsDataset(spark)
sp500ObservationsDS.count ()
sp500ObservationsDS.show (5)
sp500ObservationsDS.createOrReplaceTempView("sp500ObservationsTV")
spark.sql("create table sp500Observations as select * from sp500ObservationsTV")

val key = System.getenv("FRED_API_KEY")
import com.coherentlogic.fred.client.core.builders.BuilderDecorators._
import com.coherentlogic.openfigi.client.core.builders.BuilderDecorators._
import com.coherentlogic.cmr.api.builders.CMR
val cmr = new CMR ()
for (ctr <- 0 to 49) {
  cmr.fred.series.observations.withSeriesId("CSUSHPINSA").doGetAsObservationsDataset(spark)
}

spark.sql("drop table sp500Observations")

// S&P/Case-Shiller U.S. National Home Price Index (CSUSHPINSA)
// S&P/Case-Shiller 20-City Composite Home Price Index (SPCS20RNSA)
// S&P/Case-Shiller 10-City Composite Home Price Index (SPCS10RNSA)
// CBOE S&P 500 3-Month Volatility Index (VXVCLS)
// S&P/Case-Shiller GA-Atlanta Home Price Index (ATXRSA)
// Index of All Common Stock Prices, Cowles Commission and Standard and Poor's Corporation for United States (M1125AUSM343NNBR)
// Consumer Price Index for All Urban Consumers: All Items (CPIAUCSL)
// U.S. / Euro Foreign Exchange Rate (DEXUSEU)

val sp500CaseShillerUSNationalHomePriceIndexDS = cmr.fred.series.observations.withApiKey (key).withSeriesId("CSUSHPINSA").doGetAsObservationsDataset(spark)
sp500CaseShillerUSNationalHomePriceIndexDS.count()
val sp500CaseShiller20CityCompositeHomePriceIndexDS = cmr.fred.series.observations.withApiKey (key).withSeriesId("SPCS20RNSA").doGetAsObservationsDataset(spark)
sp500CaseShiller20CityCompositeHomePriceIndexDS.count()
val sp500CaseShiller10CityCompositeHomePriceIndexDS = cmr.fred.series.observations.withApiKey (key).withSeriesId("SPCS10RNSA").doGetAsObservationsDataset(spark)
sp500CaseShiller10CityCompositeHomePriceIndexDS.count()
val cboeSP5003MonthVolatilityIndexDS = cmr.fred.series.observations.withApiKey (key).withSeriesId("VXVCLS").doGetAsObservationsDataset(spark)
cboeSP5003MonthVolatilityIndexDS.count()
val sp500CaseShillerGAAtlantaHomePriceIndexDS = cmr.fred.series.observations.withApiKey (key).withSeriesId("ATXRSA").doGetAsObservationsDataset(spark)
sp500CaseShillerGAAtlantaHomePriceIndexDS.count()
val indexOfAllCommonStockPricesCowlesCommissionAndSNPCorporationForUSDS = cmr.fred.series.observations.withApiKey (key).withSeriesId("M1125AUSM343NNBR").doGetAsObservationsDataset(spark)
indexOfAllCommonStockPricesCowlesCommissionAndSNPCorporationForUSDS.count()
val consumerPriceIndexForAllUrbanConsumersAllItemsDS = cmr.fred.series.observations.withApiKey (key).withSeriesId("CPIAUCSL").doGetAsObservationsDataset(spark)
consumerPriceIndexForAllUrbanConsumersAllItemsDS.count()
val usEuroForeignExchangeRateDS = cmr.fred.series.observations.withApiKey (key).withSeriesId("DEXUSEU").doGetAsObservationsDataset(spark)
usEuroForeignExchangeRateDS.count()



val categoriesDS = cmr.fred.category.withApiKey("[redacted]").withCategoryId(125).doGetAsCategoriesDataset(spark)
categoriesDS.count ()
categoriesDS.show ()

import com.coherentlogic.wb.client.core.builders.BuilderDecorators._
import com.coherentlogic.fred.client.core.builders.BuilderDecorators._
import com.coherentlogic.openfigi.client.core.builders.BuilderDecorators._
import com.coherentlogic.treasurydirect.client.core.builders.BuilderDecorators._
import com.coherentlogic.cmr.api.decorators.SparkSessionDecorators._
import com.coherentlogic.cmr.api.builders.CMR
val cmr = new CMR ()
val dataPointsDataset = cmr.worldBank.countries("br", "gb").indicators("SP.POP.TOTL").withDate("1998:2003").doGetAsDataPointsDataset(spark)
dataPointsDataset.count ()
dataPointsDataset.show ()
val lendingTypesDataset = cmr.worldBank.lendingTypes().doGetAsLendingTypesDataset(spark)
lendingTypesDataset.count ()
lendingTypesDataset.show ()
val topicsDataset = cmr.worldBank.topics().doGetAsTopicsDataset(spark)
topicsDataset.count ()
topicsDataset.show ()
val topicsDataset13 = cmr.worldBank.topic("13").withPerPage(100).doGetAsTopicsDataset(spark)
topicsDataset13.count ()
topicsDataset13.show ()
val incomeLevelsDataset = cmr.worldBank.incomeLevels().doGetAsIncomeLevelsDataset(spark)
incomeLevelsDataset.count ()
incomeLevelsDataset.show ()
val indicatorsDataset = cmr.worldBank.indicators("NY.GDP.MKTP.CD").doGetAsIndicatorsDataset(spark)
indicatorsDataset.count ()
indicatorsDataset.show ()
val catalogSourcesDataset = cmr.worldBank.source("11").withPerPage(10).doGetAsCatalogSourcesDataset(spark)
catalogSourcesDataset.count ()
catalogSourcesDataset.show ()
val countriesDS = cmr.worldBank.countries().withPerPage(5000).withIncomeLevelAsLIC ().doGetAsCountriesDataset (spark)
countriesDS.count ()
countriesDS.show ()
val countriesDS = cmr.worldBank.countries().withISOCodes("br", "gb").doGetAsCountriesDataset (spark)
countriesDS.count ()
countriesDS.show ()
```