package com.coherentlogic

package object cmr {

  import org.slf4j.Logger
  import org.slf4j.LoggerFactory

  private final val log = LoggerFactory.getLogger("com.coherentlogic.cmr package object")

  def printAndLog (message : String) : Unit = {

    System.out.println ("-----")
    System.out.println (message)
    System.out.println ("-----")

    log.info(message)
  }
}