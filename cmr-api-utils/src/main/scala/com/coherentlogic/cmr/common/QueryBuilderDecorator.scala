package com.coherentlogic.cmr.common

import scala.collection.mutable.Seq

import com.jamonapi.Monitor
import com.jamonapi.MonitorFactory

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.coherentlogic.cmr._

trait QueryBuilderDecorator[S] {

/*   def doGetAsSeq[T] (
      target : S,
      mapper : (S) => Seq[T]
    ) : Seq[T] */

  private final val log = LoggerFactory.getLogger(classOf[QueryBuilderDecorator[S]])

  def doGetAsSeq[T] (
    source : S,
    mapper : (S) => Seq[T]
    ) : Seq[T] = {

      val monitor = MonitorFactory.start("com.coherentlogic.cmr.common.QueryBuilderDecorator.doGetAsSeq")

      val result : Seq[T] = mapper (source)

      monitor.stop()

      val message = s"Method performance: $monitor"

      printAndLog (message)

      result
    }
}