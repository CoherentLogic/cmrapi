package com.coherentlogic.cmr

import com.coherentlogic.cmr.common.QueryBuilderDecorator

import org.apache.spark.sql.Dataset

import scala.collection.mutable.MutableList

import com.coherentlogic.fred.client.core.domain.Category
import com.coherentlogic.fred.client.core.domain.Series
import com.coherentlogic.fred.client.core.domain.Seriess
import org.apache.spark.sql.SparkSession

import java.sql.Date

// value foreach is not a member of java.util.List
import scala.collection.JavaConversions._
import java.util.Collection

import com.coherentlogic.fred.client.core.domain.SortOrder
import com.coherentlogic.fred.client.core.domain.OrderBy
import com.coherentlogic.fred.client.core.builders.QueryBuilder

import com.jamonapi.Monitor
import com.jamonapi.MonitorFactory

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.coherentlogic.cmr._

package object fred {

  implicit class FREDQueryBuilderDecorator (queryBuilder : QueryBuilder)
    extends QueryBuilderDecorator[SerializableBean] {

    private final val log = LoggerFactory.getLogger(classOf[FREDQueryBuilderDecorator])

//    def doGetAsSeq[T] (mapper : (Data) => Seq[T]) : Seq[T] = {
//
//      val monitor = MonitorFactory.start(
//        "ccom.coherentlogic.cmr.fred.FREDQueryBuilderDecorator.doGetAsSeq")
//
//      val data = queryBuilder.doGetAsData ()
//
//      val mutableResult : Seq[T] = mapper (data)
//
//      monitor.stop()
//
//      val message = s"Method performance: $monitor"
//
//      printAndLog (message)
//
//      //val immutableResult = scala.collection.immutable.Seq(mutableResult : _*)
//
//      mutableResult
//    }

    // spark : SparkSession
    private def doGetAsFlatCategories () : MutableList[FlatCategories] = {

      val categories = queryBuilder.doGetAsCategories ()

      val flatCategoriesList = MutableList[FlatCategories]()

      val categoryList = categories.getCategoryList ()

      for (category <- categoryList) { 

          flatCategoriesList += new FlatCategories (
            categories.getId (),
            categories.getValue (),
            category.getId (),
            category.getValue (),
            category.getName (),
            category.getParentId ()
          )
        }

      flatCategoriesList
    }

    def doGetAsCategoriesDataset (implicit spark : SparkSession) : Dataset[FlatCategories] = {

      import spark.implicits._

      doGetAsFlatCategories ().toDS
    }

    private def doGetAsFlatObservations () : MutableList[FlatObservations] = {

      val observations = queryBuilder.doGetAsObservations ()

      val flatObservationsList = MutableList[FlatObservations]()

      val observationList = observations.getObservationList ()

      for (observation <- observationList) { 

        val value = observation.getValue ()

        flatObservationsList += new FlatObservations (
          toSQLDate (observations.getRealtimeStart ()),
          toSQLDate (observations.getRealtimeEnd ()),
          toSQLDate (observations.getObservationStart ()),
          toSQLDate (observations.getObservationEnd ()),
          asString (observations.getSortOrder ()),
          asString (observations.getOrderBy ()),
          observations.getLimit (),
          observations.getOffset (),
          asString (observations.getUnits ()),
          asString (observations.getOutputType ()),
          asString (observations.getFileType ()),
          observations.getCount (),
          toSQLDate (observation.getRealtimeStart ()),
          toSQLDate (observation.getRealtimeEnd ()),
          toSQLDate (observation.getDate ()),
          if (value == null) null else value.toString
        )
      }

      flatObservationsList
    }
  
    def doGetAsObservationsDataset (implicit spark : SparkSession) : Dataset[FlatObservations] = {

      import spark.implicits._
  
      val monitor = MonitorFactory.start("doGetAsObservationsDataset")

      val flatObservations = doGetAsFlatObservations ()

      monitor.stop

      val message = s"Method performance: $monitor"

      printAndLog (message)

      flatObservations.toDS
    }

    private def doGetAsFlatSeriess () : MutableList[FlatSeriess] = {

      val seriess : Seriess = queryBuilder.doGetAsSeriess ().asInstanceOf[Seriess]

      val flatSeriessList = MutableList[FlatSeriess]()

      val seriesList = seriess.getSeriesList ()

      for (series : Series <- seriesList) { 

        flatSeriessList += new FlatSeriess (
          toSQLDate (seriess.getRealtimeStart ()),
          toSQLDate (seriess.getRealtimeEnd ()),
          asString (seriess.getSortOrder ()),
          asString (seriess.getOrderBy ()),
          seriess.getLimit (),
          seriess.getOffset (),
          asString (seriess.getUnits ()),
          asString (seriess.getOutputType ()),
          asString (seriess.getFileType ()),
          seriess.getCount (),
          asString (seriess.getFilterVariable ()),
          asString (seriess.getFilterValue ()),
          // -----
          asString (series.getId ()),
          asString (series.getValue ()),
          toSQLDate (series.getRealtimeStart ()),
          toSQLDate (series.getRealtimeEnd ()),
          asString (series.getTitle ()),
          toSQLDate (series.getObservationStart ()),
          toSQLDate (series.getObservationEnd ()),
          asString (series.getFrequency ()),
          asString (series.getFrequencyLong ()),
          asString (series.getUnits ()),
          asString (series.getUnitsShort ()),
          asString (series.getSeasonalAdjustment ()),
          asString (series.getSeasonalAdjustmentShort ()),
          toSQLDate (series.getLastUpdated ()),
          series.getPopularity (),
          asString (series.getNotes ())
        )
      }
  
      flatSeriessList
    }

    def doGetAsSeriessDataset (implicit spark : SparkSession) : Dataset[FlatSeriess] = {

      import spark.implicits._
  
      doGetAsFlatSeriess ().toDS
    }
  }

  private def toSQLDate (date : java.util.Date) : Date = {
    return new Date (date.getTime)
  }

  private def asString (obj : Object) : String = {

    var result = if (obj == null) null else obj.toString

    result
  }
}

// https://stackoverflow.com/questions/4947535/ternary-operator-similar-to/4949295
case class StringOperators(value : String) {
  def ?[X](t: => X) = new {
    def |(f: => X) = if(value != null) t else f
  }
}

object StringOperators {
  implicit def StringOperator(value : String) = StringOperators(value) 
}

case class FlatCategories (
    categoriesId : String,
    categoriesValue : String,
    categoryId : String,
    categoryValue : String,
    categoryName : String,
    categoryParentId : String) {}

case class FlatObservations (
    observationsRealtimeStart : Date,
    observationsRealtimeEnd : Date,
    observationsObservationStart : Date,
    observationsObservationEnd : Date,
    observationsSortOrder : String,
    observationsOrderBy : String,
    observationsLimit : Long,
    observationsOffset : Integer,
    observationsUnits : String,
    observationsOutputType : String,
    observationsFileType : String,
    observationsCount : Integer,
    observationRealtimeStart : Date,
    observationRealtimeEnd : Date,
    observationDate: Date,
    // This should be a scala.math.BigDecimal however in version 2.11 of Scala, constructing this with null will cause
    // an exception to be thrown and right now I don't see a graceful way around this.
    observationValue : String
) {}

case class FlatSeriess (
    seriessRealtimeStart : Date,
    seriessRealtimeEnd : Date,
    seriessSortOrder : String,
    seriessOrderBy : String,
    seriessLimit : Long,
    seriessOffset : Integer,
    seriessUnits : String,
    seriessOutputType : String,
    seriessFileType : String,
    seriessCount : Integer,
    seriessFilterVariable : String,
    seriessFilterValue : String,
    seriesId : String,
    seriesValue : String,
    seriesRealtimeStart : Date,
    seriesRealtimeEnd : Date,
    seriesTitle : String,
    seriesObservationStart : Date,
    seriesObservationEnd : Date,
    seriesFrequency : String,
    seriesFrequencyLong : String,
    seriesUnits : String,
    seriesUnitsShort : String,
    seriesSeasonalAdjustment : String,
    seriesSeasonalAdjustmentShort : String,
    seriesLastUpdated : Date,
    seriesPopularity : Integer,
    seriesNotes : String
) {}
