package com.coherentlogic.coherent.data.adapter.openfigi.client.core.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Configuration
@ImportResource({"classpath*:openfigi-client-configuration/aop-configuration.xml"})
public class XMLConfiguration {

}
