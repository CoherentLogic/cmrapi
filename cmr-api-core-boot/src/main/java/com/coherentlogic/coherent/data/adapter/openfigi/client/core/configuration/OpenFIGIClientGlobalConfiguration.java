package com.coherentlogic.coherent.data.adapter.openfigi.client.core.configuration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;

import com.coherentlogic.coherent.data.adapter.core.factories.TypedFactory;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.adapters.DataEntryTypeAdapter;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.adapters.DataTypeAdapter;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.adapters.ErrorEntryTypeAdapter;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.adapters.MappingResponseTypeAdapter;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.builders.QueryBuilder;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.Data;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.DataEntry;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.ErrorEntry;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.MappingResponse;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.MappingValue;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.extractors.DataExtractor;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.extractors.MappingResponseExtractor;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.factories.DataEntryFactory;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.factories.DataFactory;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.factories.ErrorEntryFactory;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;

/**
 * 
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Configuration
public class OpenFIGIClientGlobalConfiguration {

    public static final String
        OPEN_FIGI_REST_TEMPLATE = "openFIGIRestTemplate",
        OPEN_FIGI_QUERY_BUILDER = "openFIGIQueryBuilder",
        GSON_BUILDER = "gsonBuilder", VERSION = "version";

    @Bean(name=OPEN_FIGI_REST_TEMPLATE)
    public RestTemplate getRestTemplate () {

        RestTemplate restTemplate = new RestTemplate();

        return restTemplate;
    }

    @Bean(name=OPEN_FIGI_QUERY_BUILDER)
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public QueryBuilder getQueryBuilder (
        @Qualifier(OPEN_FIGI_REST_TEMPLATE) RestTemplate restTemplate,
        @Qualifier(DataExtractor.BEAN_NAME) ResponseExtractor<Data> dataExtractor,
        @Qualifier(MappingResponseExtractor.BEAN_NAME) ResponseExtractor<MappingResponse>
            mappingResponseExtractor
    ) {
        return new QueryBuilder (restTemplate, dataExtractor, mappingResponseExtractor);
    }

    @Bean(name=DataExtractor.BEAN_NAME)
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public ResponseExtractor<Data> getDataExtractor (
        @Qualifier(GSON_BUILDER) GsonBuilder gsonBuilder,
        @Qualifier(DataTypeAdapter.BEAN_NAME) TypeAdapter<Data> dataTypeAdapter
    ) {
        return new DataExtractor(gsonBuilder, dataTypeAdapter);
    }

    @Bean(name=MappingResponseExtractor.BEAN_NAME)
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public ResponseExtractor<MappingResponse> getMappingResponseExtractor (
        @Qualifier(GSON_BUILDER) GsonBuilder gsonBuilder,
        @Qualifier(MappingResponseTypeAdapter.BEAN_NAME) TypeAdapter<MappingResponse>
            mappingResponseTypeAdapter
    ) {
        return new MappingResponseExtractor(gsonBuilder, mappingResponseTypeAdapter);
    }

    @Bean(name=MappingResponseTypeAdapter.BEAN_NAME)
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public TypeAdapter<MappingResponse> getMappingResponseTypeAdapter (
        @Qualifier(GSON_BUILDER) GsonBuilder gsonBuilder
    ) {
        return new MappingResponseTypeAdapter(gsonBuilder);
    }

    @Bean(name=DataTypeAdapter.BEAN_NAME)
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public TypeAdapter<Data> getDataTypeAdapter (
        @Qualifier(OpenFIGIClientGlobalConfiguration.GSON_BUILDER) GsonBuilder gsonBuilder,
        @Autowired TypedFactory<Data> dataFactory,
        @Qualifier(DataEntryTypeAdapter.BEAN_NAME) DataEntryTypeAdapter dataEntryTypeAdapter,
        @Qualifier(ErrorEntryTypeAdapter.BEAN_NAME) ErrorEntryTypeAdapter errorEntryTypeAdapter
    ) {
        return new DataTypeAdapter(gsonBuilder, dataFactory, dataEntryTypeAdapter, errorEntryTypeAdapter);
    }

    @Bean(name=DataFactory.BEAN_NAME)
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public TypedFactory<Data> getDataFactory () {
        return new DataFactory();
    }

    @Bean(name=DataEntryFactory.BEAN_NAME)
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public TypedFactory<DataEntry> getDataEntryFactory () {
        return new DataEntryFactory();
    }

    @Bean(name=ErrorEntryFactory.BEAN_NAME)
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public TypedFactory<ErrorEntry> getErrorEntryFactory () {
        return new ErrorEntryFactory();
    }

    @Bean(name=DataEntryTypeAdapter.BEAN_NAME)
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public DataEntryTypeAdapter getDataEntryTypeAdapter (
        @Qualifier(DataEntryFactory.BEAN_NAME) TypedFactory<DataEntry> dataEntryFactory
    ) {
        return new DataEntryTypeAdapter (dataEntryFactory);
    }

    @Bean(name=ErrorEntryTypeAdapter.BEAN_NAME)
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public ErrorEntryTypeAdapter getErrorEntryTypeAdapter (
        @Qualifier(ErrorEntryFactory.BEAN_NAME) TypedFactory<ErrorEntry> errorEntryFactory
    ) {
        return new ErrorEntryTypeAdapter (errorEntryFactory);
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public Data getData () {
        return new Data ();
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public DataEntry getDataEntry () {
        return new DataEntry ();
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public ErrorEntry getErrorEntry () {
        return new ErrorEntry ();
    }

    @Bean(name=GSON_BUILDER)
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public GsonBuilder getGsonBuilder () {
        return new GsonBuilder ();
    }
}
