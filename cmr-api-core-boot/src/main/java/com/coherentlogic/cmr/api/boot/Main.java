package com.coherentlogic.cmr.api.boot;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

import com.coherentlogic.cmr.services.GoogleAnalyticsMeasurementService;
import com.coherentlogic.coherent.data.adapter.core.util.WelcomeMessage;

/**
 * 
 */
@SpringBootApplication(
//    exclude = { // Not necessary when running in Spark.
//        DataSourceAutoConfiguration.class,
//        DataSourceTransactionManagerAutoConfiguration.class,
//        HibernateJpaAutoConfiguration.class
//    }
)
@EnableAutoConfiguration(
//    exclude = {
//        DataSourceAutoConfiguration.class,
//        DataSourceTransactionManagerAutoConfiguration.class,
//        HibernateJpaAutoConfiguration.class
//    }
)
@ComponentScan(
  basePackages= {
    "com.coherentlogic.coherent.data.adapter.openfigi.client.core.configuration",
    "com.coherentlogic.treasurydirect.client.core.factories",
    "com.coherentlogic.treasurydirect.client.core.configuration"
  })
@ImportResource({
//    "classpath*:infinispan-cache-configuration/infinispan-application-context.xml",
    "classpath*:lei-client-configuration/application-context.xml",
    "classpath*:fred-client-configuration/application-context.xml",
    "classpath*:wb-client-configuration/application-context.xml",
    "classpath*:quandl-client-configuration/application-context.xml"
})
public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    private static GoogleAnalyticsMeasurementService googleAnalyticsMeasurementService
        = new GoogleAnalyticsMeasurementService ();

    static {

        final List<String> text = Arrays.asList(
            new String[] {
"                                                                                                          ",
" CMR: Rapid Data Acquisition for the Apache Spark Cluster Computing Platform version 2.0.3.2-RELEASE      ",
"                                                                                                          ",
" BE ADVISED:                                                                                              ",
"                                                                                                          ",
" This framework uses the Google Analytics Measurement API (GAMA) to track framework usage information.    ",
" As this software is open-source, you are welcomed to review our use of GAMA and feel free to send an     ",
" email to support@coherentlogic.com if you have further questions.                                        ",
"                                                                                                          ",
" We do NOT recommend disabling this feature however we offer the option below, just add the following VM  ",
" parameter and tracking will be disabled:                                                                 ",
"                                                                                                          ",
" -DGOOGLE_ANALYTICS_TRACKING=false                                                                        ",
"                                                                                                          ",
" The CMR source code can be found here:                                                                   ",
"                                                                                                          ",
" https://bitbucket.org/CoherentLogic/cmrapi/src/master/cmr-api-core-boot/                                 ",
"                                                                                                          ",
" See:                                                                                                     ",
" * com.coherentlogic.cmr.services.GoogleAnalyticsMeasurementService                                       ",
" * com.coherentlogic.cmr.api.boot.Main                                                                    ",
"                                                                                                          ",
" https://bitbucket.org/CoherentLogic/coherent-logic-enterprise-data-adapter/                              ",
"                                                                                                          ",
" See:                                                                                                     ",
" * com.coherentlogic.coherent.data.adapter.core.services.AbstractGoogleAnalyticsMeasurementService        ",
"                                                                                                          ",
" https://bitbucket.org/CoherentLogic/google-analytics-measurement-api-client/                             ",
"                                                                                                          ",
" See the entire project.                                                                                  ",
"                                                                                                          "
            }
        );

        WelcomeMessage message = new WelcomeMessage (text);

        message.display();

        if (googleAnalyticsMeasurementService.shouldTrack ()) {
            try {
                googleAnalyticsMeasurementService.fireGAFrameworkUsageEvent ();
            } catch (Throwable cause) {
                log.warn ("The googleAnalyticsMeasurementService.fireGAFrameworkUsageEvent method threw an exception."
                    + " This exception has been caught and can be ignored and the CMR API will continue to function"
                    + " normally.",
                    cause);
            }
        } else
            log.warn("The shouldTrack method returned false and hence the fireGAFrameworkUsageEvent"
                + "method was not invoked.");
    }

    @Autowired
    private ConfigurableApplicationContext applicationContext;

    public static Main initialize () {

        SpringApplicationBuilder builder = new SpringApplicationBuilder(Main.class);

        ConfigurableApplicationContext applicationContext =
            builder.web(false).headless(false).registerShutdownHook(true).run();//;new String[]{"debug"});

        Main result = applicationContext.getBean(Main.class);

        return result;
    }

    public <T> T getBean (Class<T> type) {

        if (applicationContext == null)
            throw new NullPointerException ("The applicationContext is null which implies the initialize "
                + "method was not called");

        return applicationContext.getBean(type);
    }

    @PreDestroy
    public void stop () {
        applicationContext.stop();
        applicationContext.close();
    }

    public static void main (String[] args) {

//        String master = "local[*]";
//
//        SparkConf sparkConf = new SparkConf()
//            .setAppName(Main.class.getName())
//            .setMaster(master);
//
//        JavaSparkContext javaSparkContext = new JavaSparkContext(sparkConf);
//
//        SparkSession sparkSession = new SparkSession (javaSparkContext.sc());

// ---------------------------------------------------------------------------------------------------------------------

        Main main = Main.initialize();

        main.getBean(com.coherentlogic.coherent.data.adapter.lei.client.core.builders.QueryBuilder.class);
        
//        com.coherentlogic.wb.client.core.builders.QueryBuilder worldBankQueryBuilder = main.getBean(com.coherentlogic.wb.client.core.builders.QueryBuilder.class);
//
//        DataPoints dataPoints = worldBankQueryBuilder.countries("br", "gb").indicators("SP.POP.TOTL").withDate("1998:2003").doGetAsDataPoints();
//
//        System.out.println ("dataPoints: " + dataPoints);
//
        com.coherentlogic.coherent.data.adapter.openfigi.client.core.builders.QueryBuilder
            openFIGIQueryBuilder = main.getBean(com.coherentlogic.coherent.data.adapter.openfigi.client.core.builders.QueryBuilder.class);
//
//        Data data = openFIGIQueryBuilder
//          .getRequestBody ()
//          .withFinancialInstrumentGlobalIdentifier ("BBG00K62SK81")
//          .done ().doGetAsData();
//
//        System.out.println ("data: " + data);
//
//        com.coherentlogic.fred.client.core.builders.QueryBuilder fredQueryBuilder = main.getBean(com.coherentlogic.fred.client.core.builders.QueryBuilder.class);
//
//        Categories categories = fredQueryBuilder.category().withCategoryId(125).doGetAsCategories();
//
//        System.out.println ("categories: " + categories);
////        Categories categories = builder.newQueryBuilder().category().withCategoryId(125).doGetAsCategories();
////
////        Dataset<Categories> categoriesDataset = builder.newCategoriesDataset(sparkSession, categories);
////
////        String[] columns = categoriesDataset.columns();
////
////        for (String column : columns)
////            log.info("column: " + column);
//
//// ---------------------------------------------------------------------------------------------------------------------
//
//        com.coherentlogic.treasurydirect.client.core.builders.QueryBuilder treasuryDirectQueryBuilder =
//            main.getBean(com.coherentlogic.treasurydirect.client.core.builders.QueryBuilder.class);
//
//        Debts debts = treasuryDirectQueryBuilder.doGetAsDebts();
//
//        System.out.println ("debts: " + debts);
//
//        sparkSession.close();
//
//        javaSparkContext.close ();

        System.exit (9999);
    }
}
